package com.twij.patientcare.service;

import com.twij.patientcare.exception.UserNotFoundException;
import com.twij.patientcare.model.Patient;
import com.twij.patientcare.repo.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class PatientService {
    private final PatientRepo patientRepo;

    @Autowired
    public PatientService(PatientRepo patientRepo) {
        this.patientRepo = patientRepo;
    }

    public Patient addPatient(Patient patient){
        patient.setPatientCode(UUID.randomUUID().toString());
        return patientRepo.save(patient);
    }

    public List<Patient> findAllPatients(){
        return patientRepo.findAll();
    }

    public Patient updatePatient(Patient patient){
        return patientRepo.save(patient);
    }

    public Patient findPatientById(int id){
        return patientRepo.findPatientById(id)
                .orElseThrow( () -> new UserNotFoundException("Użytkownik o  id " + id + " nie został odnaleziony"));
    }

    public void deletePatient(int id){
        patientRepo.deletePatientById(id);
    }

}
