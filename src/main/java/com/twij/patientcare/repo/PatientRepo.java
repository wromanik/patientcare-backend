package com.twij.patientcare.repo;

import com.twij.patientcare.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface PatientRepo extends JpaRepository<Patient, Integer> {

    void deletePatientById(int id);
    Optional<Patient> findPatientById(int id);
}
